var user = {
    base_url: null,
    init: function () {
        this.bind_events();
        this.loadusers();
    },
    bind_events: function () {
        var self = this;
        $('#user_password').hide()
        $('#icon').hide()
        $("#login").on("click", () => {
            var mobile_number = $('#mobile_number').val();
            if (mobile_number) {
                $.ajax({
                    type: 'POST',
                    url: 'http://localhost:5002/check_unique_mobile_number',
                    datatype: 'json',
                    data: {
                        mobile_number: mobile_number
                    },
                    success: (data) => {
                        if (data.message == 'error') {
                            $('#icon').hide()
                            $('#user_password').hide()
                        } else {
                            $('#icon').show()
                            $('#user_password').show()
                        }
                    }
                })
            }

        });
        $("#login").on("click", () => {
            var mobile_number = $('#mobile_number').val();
            var password = $('#user_password').val();
            $.ajax({
                type: 'POST',
                url: 'http://localhost:5002/user_login',
                datatype: 'json',
                data: {
                    mobile_number: mobile_number,
                    password: password
                },
                success: (data) => {
                    let token = {
                        token: data.token,
                        id: data.payload.id
                    }
                    localStorage.setItem('Authorization', JSON.stringify(token))
                    let user_token = JSON.parse(localStorage.getItem('Authorization'))
                    let header = new Headers();
                    header.append('Authorization', `Bearer ${user_token}`)
                    if (data.success == true) {
                        if (data.payload.user_role === 'admin') {
                            window.location.href = '/admin_dashboard';
                        }
                        if (data.payload.user_role === 'developer') {
                            window.location.href = '/master_dashboard';
                        }
                    } else {
                        alert("error")
                    }
                }
            })
        });
    },
    loadusers: function() {
        var self = this;
        $.ajax({
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: "http://localhost:5002/get_all_user_info",
            success: function (data) {
                $('#users_table').DataTable({
                    data: data,
                })
            }
        });
    }
}