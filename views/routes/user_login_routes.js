const express = require("express")
const router = express.Router()
const userController = require('../../server/controller/userController')

// index page
router.get('/', function (req, res) {
    res.render('pages/login');
});

router.get('/login_with_navbar', function (req, res) {
    res.render('pages/login_with_navbar');
});

router.get('/index', function (req, res) {
    res.render('pages/index');
});


router.get('/forgot_password', function (req, res) {
    res.render('pages/forgot_password');
});

router.get('/change_password', function (req, res) {
    res.render('pages/change_password');
});

router.get('/verify_forgot_password', function (req, res) {
    res.render('pages/verify_forgot_password');
});

router.get('/admin_dashboard', function (req, res) {
    res.render('pages/admin_dashboard');
});

router.get('/master_dashboard', function (req, res) {
    res.render('pages/master_dashboard');
});
router.get('/table', function (req, res) {
    res.render('pages/table');
});
router.get('/update_profile', function (req, res) {
    res.render('pages/update_profile');
});
router.get('/change_password', function (req, res) {
    res.render('pages/change_password');
});
router.get('/change_password', function (req, res) {
    res.render('pages/change_password');
});



router.get('/get_user_info', userController.get_current_user_info);

router.post('/get_all_user_info', userController.get_all_user_info);

router.post('/add_user', userController.add_user)

router.post('/check_unique_mobile_number', userController.check_unique_mobile_number)

router.post('/verify_forgot_password', userController.verify_forgot_password)

router.post('/update_user_password', userController.update_user_password)

router.post('/update_user_profile', userController.update_user_profile)

router.post('/user_login', userController.user_login)

router.post('/send_otp', userController.send_otp)

module.exports = router