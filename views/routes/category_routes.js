const express = require("express")
const router = express.Router()
const categoryController = require('../../server/controller/categoryController')

router.post('/add_category',categoryController.add_category)

router.get('/get_category',categoryController.get_category)

router.post('/update_category',categoryController.update_category)

module.exports = router