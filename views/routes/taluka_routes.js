const express = require("express")
const router = express.Router()
const talukaController = require('../../server/controller/talukaController')

router.post('/add_taluka',talukaController.add_taluka)

router.get('/get_all_taluka',talukaController.get_all_taluka)

router.post('/get_taluka_by_id',talukaController.get_taluka_by_id)

module.exports = router