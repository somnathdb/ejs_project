const express = require("express")
const router = express.Router()
const user_roleController = require('../../server/controller/user_roleController')

router.post('/create_user_role',user_roleController.add_user_role)

router.get('/get_user_role',user_roleController.get_all_user_role)

router.post('/update_user_access',user_roleController.update_user_access)

module.exports = router