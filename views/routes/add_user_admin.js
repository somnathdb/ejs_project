const express = require("express")
const router = express.Router()

router.get('/', function (req, res) {
  res.render('pages/add_user_admin');
});

module.exports = router;