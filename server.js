var express = require('express');
var app = express();
var path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')
const cookieParser = require("cookie-parser");
const passport = require("passport");
const session = require("express-session");
const db = require('./server/config/keys').mongoURL;

app.locals.baseURL = "http://localhost:5002";

mongoose.connect(db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}).then(() => {
  console.log('Database sucessfully connected');
}).catch((err) => {
  console.log(err);
});

app.use(cookieParser());
app.use(
  session({
    secret: "secretkey14555444",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
require("./server/config/passport")(passport);

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json());

const userRoutes = require('./views/routes/user_login_routes')
const user_role_Routes = require('./views/routes/user_role_routes')
const admin_routes = require('./views/routes/add_user_admin')
const talukaRoutes = require('./views/routes/taluka_routes')
const categoryRoutes = require('./views/routes/category_routes')

app.use('/', userRoutes);
app.use('/add_user_admin', admin_routes)
app.use('/role', user_role_Routes)
app.use('/taluka', talukaRoutes)
app.use('/category', categoryRoutes)

app.use(express.static('app-assets'))
app.use('/images', express.static(__dirname + 'app-assets/images'))
app.use('/css', express.static(__dirname + 'app-assets/css'))
app.use('/vendors', express.static(__dirname + 'app-assets/vendors'))
app.use('/js', express.static(__dirname + 'app-assets/js'))

app.use(express.static(path.join(__dirname, 'public')));


app.listen(5002);
console.log('Server is listening on port 5002');
