const mongoose = require('mongoose')
const Schema = mongoose.Schema

const talukaSchema = new Schema({
    taluka_name: {
        type: String
    },
    pincode: {
        type: String
    },
    state: {
        type: String
    },
    district: {
        type: String
    }
}, {
    timestamps: true
});



module.exports = mongoose.model('taluka', talukaSchema)