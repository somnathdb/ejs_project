const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userroleSchema = new Schema({
    user_role: {
        type: String,
        default:'admin'
    },
    user_access:{
        type:String,
        dafault:"All"
    }
},{ timestamps: true });



module.exports = mongoose.model('user_role', userroleSchema)