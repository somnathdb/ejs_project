const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require("bcryptjs");
const SALT_I = 10;
var uuid = require('node-uuid');

const userSchema = new Schema({
    _id: {
        type: String,
        default:uuid.v1
    },
    name: {
        type: String
    },
    mobile_number: {
        type: String,
        maxlength: 10,
        require: true
    },
    email: {
        type: String
    },
    user_role: {
        type: String
    },
    address: {
        type: String
    },
    is_active: {
        type: Boolean
    },
    password: {
        type: String
    },
    otp: {
        type: Number,
        maxlength: 4
    },
    profile_image:{
        type:String
    }
}, {
    timestamps: true
});

userSchema.pre("save", function (next) {
    var user = this;
    if (user.isModified("password")) {
        bcrypt.genSalt(SALT_I, function (err, salt) {
            if (err) {
                return next(err);
            }
            console.log(user.password)
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

userSchema.methods.change_password = async (candidatePassword) => {
    //Creating a Hash
    var salt = bcrypt.genSaltSync(SALT_I);
    var hash = bcrypt.hashSync(candidatePassword, salt);
    return hash;
};



module.exports = mongoose.model('user', userSchema)