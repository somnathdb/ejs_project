const userModel = require('../models/userModel')
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const keys = require('../config/keys').secretIOkey;

exports.add_user = async (req, res, next) => {
    try {
        const body = req.body
        let new_User = new userModel({
            name: body.name,
            mobile_number: body.mobile_number,
            email: body.email,
            user_role: body.user_role,
            address: body.address,
            is_active: body.is_active,
            password: body.password,
            otp: body.otp,
            profile_image:body.profile_image
        })
        const create_user = new_User.save()
        if (create_user) {
            res.status(201).json({
                title: "Success",
                message: "User Successfully Added"
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.check_unique_mobile_number = async (req, res, next) => {
    try {
        const body = req.body;
        let check_unique_mobile_number = await userModel.findOne({
            mobile_number: body.mobile_number,
        });
        if (check_unique_mobile_number) {
            res.status(200).json({
                title: "Success",
                message: "Success"
            })
        } else {
            res.status(201).json({
                title: "Error",
                message: "Mobile Number Already registered"
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
};


exports.verify_forgot_password = async (req, res, next) => {
    try {
        const body = req.body;
        let forgot_user_password = await userModel.findOne({
            mobile_number: body.mobile_number,
            otp: body.otp,
        });
        if (forgot_user_password) {
            return res.status(200).json({
                title: "Success",
                message: "User Password Successfully Verify"
            });
        } else {
            return res.status(500).json({
                title: "Error",
                message: "Something Wrong"
            });
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
};



exports.update_user_password = async (req, res, next) => {
    try {
        const body = req.body;
        const user_id = req.headers.authorization
        let userdata = await userModel.findOne({
            _id: user_id
        });
        if (body.new_password === body.confirm_password && userdata._id) {
            var data = await userModel.findOneAndUpdate({
                password: body.password
            }, {
                $set: {
                    password: body.new_password
                },
            });
            if (data) {
                return res.status(200).json({
                    title: "Success",
                    message: "User Password Successfully Updated"
                });
            }
        } else {
            return res.status(401).json({
                title: "Erroe",
                message: "New password and confirm password is Mismatch"
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
};

exports.update_user_profile = async (req, res, next) => {
    try {
        // console.log(req.body)
        const body = req.body;
        var data = await userModel.findOneAndUpdate({
            _id: body._id
        }, {
            $set: {
                name: body.name,
                mobile_number: body.mobile_number,
                email: body.email,
                user_role: body.user_role,
                address: body.address,
                is_active: body.is_active,
                otp: body.otp
            },
        });
        if (data) {
            return res.status(200).json({
                title: "Success",
                message: "Profile Successfully Updated"
            });

        } else {
            return res.status(401).json({
                title: "Error",
                message: "Something Went Wrong"
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
};

exports.user_login = (req, res, next) => {
    const body = req.body
    const mobile_number = body.mobile_number;
    const password = body.password;
    userModel.findOne({
        mobile_number
    }).then(user => {
        if (!user) {
            return res.status(404).json({
                message: "Error",
                mobile_number: 'Mobile Number Not Found'
            });
        }
        bcrypt.compare(password, user.password).then(isMatch => {
            if (isMatch) {
                const payload = {
                    id: user.id,
                    email: user.email,
                    mobile_number: user.mobile_number,
                    user_role: user.user_role
                }
                jwt.sign(
                    payload,
                    keys, {
                        expiresIn: 3600,
                    },
                    (err, token) => {
                        res.json({
                            payload: payload,
                            success: true,
                            token: "Bearer " + token,
                        });
                    }
                );
            } else {
                return res.status(400).json({
                    title: "Error",
                    message: 'password Incurrect'
                });
            }
        });
    });
}

exports.send_otp = async (req, res, next) => {
    try {
        let send_otp = true
        if (send_otp = true) {
            return res.status(200).json({
                title: "Success",
                message: "OTP Successfully Send"
            });
        } else {
            return res.status(500).json({
                title: "Error",
                message: "Something Wrong"
            });
        }
        //}
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
};

exports.get_current_user_info = async (req, res, next) => {
    try {
        console.log(req.query)
        const user_id = req.headers.authorization
        let data = await userModel.findOne({
            _id: req.query.user_id
        });
        return res.status(200).json({
            title: "Success",
            message: "Current User Found",
            result:data
        });
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.get_all_user_info = async (req, res, next) => {
    try {
        const body = req.body
        const limit = parseInt(body.limit)
        let data = await userModel.find().skip(limit * (body.pageNo - 1)).limit(limit);
        return res.status(200).json({
            title: "Success",
            message: "Found all users",
            result:data
        });
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}