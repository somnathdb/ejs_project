const categoryModel = require('../models/categoryModel')

exports.add_category = async (req, res, next) => {
    try {
        const body = req.body
            let new_category = new categoryModel({
                category_name: body.category_name
            })
            const create_category = new_category.save()
            if (create_category) {
                res.status(201).json({
                    title: "Success",
                    message: "Category Successfully Added"
                })
            }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.get_category = async (req, res, next) => {
    try {
        let category = await categoryModel.find()
        if (category) {
            res.status(201).json({
                title: "Success",
                message: "Found All Category",
                result:data
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.update_category = async (req, res, next) => {
    try {
        const body = req.body
        var data = await categoryModel.findOneAndUpdate({
            _id: body.id
        }, {
            $set: {
                category_name: body.category_name
            }
        });
        if (data) {
            return res.status(200).json({
                title: "Success",
                message: "Category Successfully Updated"
            });
        }

    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}