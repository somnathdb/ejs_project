const talukaModel = require('../models/taluka_Model')

exports.add_taluka = async (req, res, next) => {
    try {
        const body = req.body
        let new_taluka = new talukaModel({
            taluka_name: body.taluka_name,
            pincode: body.pincode,
            state: body.state,
            district: body.district
        })
        const create_taluka = new_taluka.save()
        if (create_taluka) {
            res.status(201).json({
                title: "Success",
                message: "User Role Successfully Added"
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.get_all_taluka = async (req, res, next) => {
    try {
        let data = await talukaModel.find();
        return res.status(200).json({
            title: "Success",
            message: "Found All Taluka",
            result:data
        });
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.get_taluka_by_id = async (req, res, next) => {
    try {
        const body = req.body
        let data = await talukaModel.findById({
            _id:body.id
        });
        return res.status(200).json({
            title: "Success",
            message: "Found Taluka",
            result:data
        });
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}
