const userroleModel = require('../models/userRole')

exports.add_user_role = async (req, res, next) => {
    try {
        const body = req.body
        const user_role = userroleModel.find({
            user_role: body.user_role
        })
        if (user_role.user_role === body.user_role) {
            res.status(500).json({
                title: "Error",
                message: "User Role Already Exites"
            })
        } else {
            let new_user_role = new userroleModel({
                user_role: body.user_role
            })
            const create_user_role = new_user_role.save()
            if (create_user_role) {
                res.status(201).json({
                    title: "Success",
                    message: "User Role Successfully Added"
                })
            }
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.get_all_user_role = async (req, res, next) => {
    try {
        let user_role = await userroleModel.find()
        if (user_role) {
            res.status(201).json({
                title: "Success",
                message: "Found All User Roles",
                result:user_role
            })
        }
    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}

exports.update_user_access = async (req, res, next) => {
    try {
        const body = req.body
        var data = await userroleModel.findOneAndUpdate({
            _id: body.id
        }, {
            $set: {
                user_access: body.user_access
            }
        });
        if (data) {
            return res.status(200).json({
                title: "Success",
                message: "User Access Successfully Updated"
            });
        }

    } catch (err) {
        return res.status(500).json({
            title: "Error",
            message: "Internal Server Error"
        })
    }
}